@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="{{ asset('assets/css/jquery.fileupload.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

<div class="container">

    <h1 class="page-header">Upload Entries <button type="button" class="btn btn-link add-entry"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add entry</button></h1>

    <div class="row">
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th class="column-filename">Filename</th>
                    <th style="text-align: center;">Status</th>
                    <th class="column-createdat">Uploaded</th>
                </tr>
            </thead>
            <tbody>
                
                <?php $id = ($entries->currentPage() - 1) * $entries->perPage() + 1; ?>

                @foreach ($entries as $entry)
                    <tr>
                        <td class="column-id" style="vertical-align: middle !important; text-align: center;">{{ $id }}</td>
                        <td class="column-title" style="vertical-align: middle !important;"><a class="title-link" href="{{ url('entries/' . $entry->id) }}">{{ $entry->title }}</a></td>
                        <td class="column-filename" style="vertical-align: middle !important;">{{ $entry->ori_filename }}</td>
                        <td class="column-progress">
                            <table class="table-condensed" style="font-size: 0.9em;">
                                <tr>
                                    <td style="text-align: center;"><img height="25" src="{{ asset('assets/images/ooyala.gif') }}"></td>
                                    <td width="100px" class="status" style="text-align: center;">
                                        @if ($entry->ooyala->status == 'pending')
                                            <span class="label label-default">Pending</span>

                                        @elseif ($entry->ooyala->status == 'processing')
                                            <span class="label label-info">Processing</span>

                                        @elseif ($entry->ooyala->status == 'success')
                                            <span class="label label-success">Success</span>

                                        @else
                                            <span class="label label-danger">Failed</span>
                                        @endif
                                    </td>
                                    <td class="subcolumn-progress" width="100px">
                                        <div class="progress" style="height:10px; margin-bottom: 0 !important;">
                                          <div class="progress-bar progress-bar-success progress-bar-striped {{ $entry->ooyala->status }}@if ($entry->ooyala->status == 'processing') active @endif" role="progressbar" aria-valuenow="{{ $entry->ooyala->progress }}" aria-valuemin="0" aria-valuemax="100" data-id="{{ $entry->id }}" data-type="ooyala" style="width: {{ $entry->ooyala->progress }}%">
                                          </div>
                                        </div>
                                    </td>
                                    <td class="percentage">{{ $entry->ooyala->progress }}%</td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;"><img height="30" src="{{ asset('assets/images/youtube.gif') }}"></td>
                                    <td width="100px" class="status" style="text-align: center;">
                                        @if ($entry->youtube->status == 'pending')
                                            <span class="label label-default">Pending</span>

                                        @elseif ($entry->youtube->status == 'processing')
                                            <span class="label label-info">Processing</span>

                                        @elseif ($entry->youtube->status == 'success')

                                            <span class="label label-success">Success</span>
                                        @else
                                            <span class="label label-danger">Failed</span>
                                        @endif
                                    </td>
                                    <td class="subcolumn-progress" width="100px">
                                        <div class="progress" style="height:10px; margin-bottom: 0 !important;">
                                          <div class="progress-bar progress-bar-success progress-bar-striped {{ $entry->youtube->status }}@if ($entry->youtube->status == 'processing') active @endif" role="progressbar" aria-valuenow="{{ $entry->youtube->progress }}" aria-valuemin="0" aria-valuemax="100" data-id="{{ $entry->id }}" data-type="youtube" style="width: {{ $entry->youtube->progress }}%">
                                          </div>
                                        </div>
                                    </td>
                                    <td class="percentage">{{ $entry->youtube->progress }}%</td>
                                </tr>
                            </table>
                        </td>
                        <td class="column-createdat" style="vertical-align: middle !important;">{{ $entry->created_at->diffForHumans() }}</td>
                    </tr>
                <?php $id++; ?>
                @endforeach
            </tbody>
        </table>

    </div>

    <div class="row">
        <div class="pagination-centered">
            {{ $entries->render() }}
        </div>
    </div>

    <div id="uploadModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Upload video</h4>
                </div>
                <div class="modal-body">
                    <form id="uploadform" action="{{ action('EntriesController@upload') }}" method="POST" enctype="multipart/form-data">

                        <div class="form-group">
                            <label class="control-label">Title (Required | No more than 100 characters)</label>
                            <input id="title" class="form-control" type="text" name="title" maxlength="100" value="">
                        </div>

                        <div class="form-group">
                            <label class="control-label">Description (Optional | No more than 5000 characters)</label><br>
                            <textarea id="description" class="form-control" name="description" rows="4" cols="50" maxlength="5000" value=""></textarea>
                        </div>

                        <div class="form-group">
                            <span class="btn btn-warning fileinput-button" style="margin-bottom: 20px;">
                                <i class="glyphicon glyphicon-play-circle"></i>
                                <span>Pick a video</span>
                                <input id="fileupload" type="file" name="file">
                            </span>
                        </div>

                        <!-- Drop Zone -->
                        <div class="upload-drop-zone" id="drop-zone">
                        or just drag and drop your file here

                        </div>

                        <!-- The container for the progress bar -->
                        <div class="progress progress-upload" style="display: none; margin-bottom: 10px;">
                          <div class="progress-bar progress-bar-default progress-bar-striped progress-upload-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                            0%
                          </div>
                        </div>

                        <!-- The container for the uploaded files -->
                        <div id="files" class="files" style="margin-bottom: 10px;"></div>

                        <!-- The container for the abort button -->
                        <div id="abort" class="abort" style="margin-bottom: 10px;"></div>
                        
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    </form>
                </div>
                <div class="modal-footer">
                    <!-- The container for the submit button -->
                    <div id="upload" class="upload pull-right" style="margin-bottom: 10px;">
                        <button class="btn btn-info">I'm done. Lets go!</button>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
</div>

<script src="{{ asset('assets/js/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('assets/js/jquery.iframe-transport.js') }}"></script>
<script src="{{ asset('assets/js/jquery.fileupload.js') }}"></script>
<script src="{{ asset('assets/js/default.js') }}"></script>
@endsection