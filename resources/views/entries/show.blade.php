@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row">

    	<div class="col-md-7">
    	
	    	<h3>{{ $entry->title }}</h3>

			<dl>
				<dd>{{ $entry->description }}</dd>
			</dl>

	    	<table class="table table-condensed">
	    		<tr class="warning">
	    			<td colspan="2">File Details</td>
	    		</tr>
	    		<tr>
	    			<td>Filename</td>
	    			<td>{{ $entry->ori_filename }}</td>
	    		</tr>
	    		<tr>
	    			<td>System Filename</td>
	    			<td><samp>{{ $entry->filename }}</samp></td>
	    		</tr>
	    		<tr>
	    			<td>Size</td>
	    			<td>{{ Human::size($entry->size) }}</td>
	    		</tr>
	    		<tr>
	    			<td>Submitted</td>
	    			<td>{{ $entry->created_at }}</td>
	    		</tr>

	    		<tr><td colspan="2">&nbsp;</td></tr>
	    		
	    		<tr class="warning">
	    			<td colspan="2"><img height="20" src="{{ asset('assets/images/ooyala.gif') }}"> Ooyala</td>
	    		</tr>
	    		<tr>
	    			<td>Status</td>
	    			<td>
	    				@if ($entry->ooyala->status == 'pending')
                            <span class="label label-default">Pending</span>

                        @elseif ($entry->ooyala->status == 'processing')
                            <span class="label label-info">Processing</span>

                        @elseif ($entry->ooyala->status == 'success')

                            <span class="label label-success">Success</span>
                        @else
                            <span class="label label-danger">Failed</span>
                        @endif
	    			</td>
	    		</tr>
	    		<tr>
	    			<td>Remarks</td>
	    			<td>{{ $entry->ooyala->remarks }}</td>
	    		</tr>
	    		<tr>
	    			<td>Embed Code</td>
	    			<td><samp>{{ $entry->ooyala->asset_id }}</samp></td>
	    		</tr>
	    		<tr>
	    			<td>Uploaded</td>
	    			<td>{{ $entry->ooyala->updated_at }}</td>
	    		</tr>

	    		<tr><td colspan="2">&nbsp;</td></tr>
	    		
	    		<tr class="warning">
	    			<td colspan="2"><img height="20" src="{{ asset('assets/images/youtube.gif') }}"> Youtube</td>
	    		</tr>
	    		<tr>
	    			<td>Status</td>
	    			<td>
	    				@if ($entry->youtube->status == 'pending')
                            <span class="label label-default">Pending</span>

                        @elseif ($entry->youtube->status == 'processing')
                            <span class="label label-info">Processing</span>

                        @elseif ($entry->youtube->status == 'success')

                            <span class="label label-success">Success</span>
                        @else
                            <span class="label label-danger">Failed</span>
                        @endif
	    			</td>
	    		</tr>
	    		<tr>
	    			<td>Remarks</td>
	    			<td>{{ $entry->youtube->remarks }}</td>
	    		</tr>
	    		<tr>
	    			<td>Embed Code</td>
	    			<td><samp>{{ $entry->youtube->asset_id }}</samp>  <a title="Watch in Youtube" target="_blank" href="https://www.youtube.com/watch?v={{ $entry->youtube->asset_id }}"><span class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a></td>
	    		</tr>
	    		<tr>
	    			<td>Uploaded</td>
	    			<td>{{ $entry->youtube->updated_at }}</td>
	    		</tr>

	    		<tr><td colspan="2">&nbsp;</td></tr>
	    		
	    		<tr class="warning">
	    			<td colspan="2"><img height="20" src="{{ asset('assets/images/gn4.png') }}"> GN4</td>
	    		</tr>
	    		<tr>
	    			<td>Status</td>
	    			<td>
	    				@if ($entry->gn4->status == 'pending')
                            <span class="label label-default">Pending</span>

                        @elseif ($entry->gn4->status == 'queued')
                            <span class="label label-info">Queued</span>

                        @elseif ($entry->gn4->status == 'success')

                            <span class="label label-success">Success</span>
                        @else
                            <span class="label label-danger">Failed</span>
                        @endif
	    			</td>
	    		</tr>
	    		<tr>
	    			<td>Remarks</td>
	    			<td>{{ $entry->gn4->remarks }}</td>
	    		</tr>
	    		<tr>
	    			<td>Object ID</td>
	    			<td><samp>{{ $entry->gn4->asset_id }}</samp></td>
	    		</tr>
	    		<tr>
	    			<td>Uploaded</td>
	    			<td>{{ $entry->gn4->updated_at }}</td>
	    		</tr>

	    	</table>
    	</div>
    </div>

    <div class="row" style="margin-top: 30px; margin-bottom: 30px;">
    	<div class="col-md-8">
	    	<a href="{{ URL::previous() }}"><button type="button" class="btn btn-success">Go Back</button></a>
	    </div>
    </div>

</div>
@endsection