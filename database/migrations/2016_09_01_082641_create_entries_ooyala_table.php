<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntriesOoyalaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries_ooyala', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('entries_id')->unique();
            $table->integer('progress')->default(0);
            $table->string('status');
            $table->string('remarks');
            $table->string('asset_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('entries_ooyala');
    }
}
