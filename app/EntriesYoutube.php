<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntriesYoutube extends Model
{
    protected $table = 'entries_youtube';

    protected $primaryKey = 'entries_id';

    public function entries()
    {
    	return $this->belongsTo('Entries');
    }
}
