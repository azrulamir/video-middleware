<?php

namespace App\Services\GN4;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use Curl;
use Log;
use Storage;
use \DOMDocument;
use App\EntriesGN4;
use \Exception;

class GN4Api
{

	/** GN4 API base URL
	 *
	 */
	public $baseUrl = 'http://gnbh.nstp.com.my/gn4';

	/**
	 * Ticket fetched from GN4 after succesful authentication
	 */
	public $ticket;

	/**
	 * Authenticate against GN4 API system
	 *
	 * @return $ticket string
	 */
	public function auth()
	{
		$response = Curl::to($this->baseUrl . '/do.ashx?cmd=login&name=ooyala&pwd=50lution5')->get();

		$xml = simplexml_load_string($response);

		if ($xml->loginResult) {
			$this->ticket = $xml->loginResult['ticket'];
			return true;
		}
		
		return false;
		
	}

	/**
	 * Create and save XML file for sending to GN4 system
	 *
	 * @param $data array
	 * @return $response string
	 */
	public function makeXML($data = array())
	{
		$dom = new DOMDocument('1.0','UTF-8');
		$dom->formatOutput = true;

		$root = $dom->createElement('video');
		$root->setAttribute('name', $data['filename'] );
		$root->setAttribute('duration', 1);

		$dom->appendChild($root);
		
		$root->appendChild( $dom->createElement('folder', '/BERITA HARIAN/Online/1.WEB-POOL') );
		$root->appendChild( $dom->createElement('caption',  $data['description']) );
		$root->appendChild( $dom->createElement('title', $data['title']) );

		// $keyword = $dom->createElement('keywords');
		// $keyword->appendChild( $dom->createElement('item', 'one') );
		// $keyword->appendChild( $dom->createElement('item', 'two') );
		// $root->appendChild( $keyword );

		// $root->appendChild( $dom->createElement('topic', '') );

		$root->appendChild( $dom->createElement('YouTubeUrl', 'https://www.youtube.com/watch?v=' . $data['youtubeid']) );
		$root->appendChild( $dom->createElement('YouTubeId', $data['youtubeid']) );

		$root->appendChild( $dom->createElement('OoyalaUrl', 'http://ooyala.com') );
		$root->appendChild( $dom->createElement('OoyalaId', $data['ooyalaid']) );

		$previewImageUrl = $dom->createElement('previewImageUrl', 'http://thumb-cdn.nst.com.my/previews/' . $data['fileid'] . '.png');
		$previewImageUrl->setAttribute('mime', 'image/png');
		$root->appendChild( $previewImageUrl );

		$thumbnailImageUrl = $dom->createElement('thumbnailImageUrl', 'http://thumb-cdn.nst.com.my/thumbnails/' . $data['fileid'] . '.png');
		$thumbnailImageUrl->setAttribute('mime', 'image/png');
		$root->appendChild( $thumbnailImageUrl );

		$dom->save(storage_path() . '/app/xml/'. $data['fileid'] .'.xml');
	}

	/**
	 * Send request to GN4 API
	 *
	 * @return $response array
	 */
	public function send($data = array())
	{
		$this->makeXML($data);
		$file = Storage::get('xml/'. $data['fileid'] .'.xml');

		if ($this->auth()) {

			$response = Curl::to($this->baseUrl . '/do.ashx?cmd=wf&name=videoFromOoyala&progress=Xml&ticket=' . $this->ticket)
		        ->withContentType('text/xml')
		        ->withData( $file )
		        ->containsFile()
		        ->post();

		    if ($response) {
		    	$xml = simplexml_load_string($response);
		    	$result = $xml->wfResult->importResult;

		    	if ($result) {
		    		$asset_id = $result['objectId'];

		    		$entry_gn4 = EntriesGN4::find($data['id']);
			        $entry_gn4->status = 'success';
			        $entry_gn4->remarks = 'Submitted to GN4 API.';
			        $entry_gn4->asset_id = $asset_id;
			        $entry_gn4->save();
		    	}
		    	else {
		    		$code = $xml->error['code'];
		    		$msg = $xml->error['message'];

		    		$entry_gn4 = EntriesGN4::find($data['id']);
			        $entry_gn4->status = 'failed';
			        $entry_gn4->remarks = $msg;
			        $entry_gn4->save();

		    		throw new Exception('Error : ' . $code . ' - ' . $msg);
		    	}
		    }
		    else {
		    	$entry_gn4 = EntriesGN4::find($data['id']);
		        $entry_gn4->status = 'failed';
		        $entry_gn4->remarks = 'Unable to submit XML file.';
		        $entry_gn4->save();

		    	throw new Exception("Unable to submit XML file.");
		    }
		}
		else {
			throw new Exception("Unable to authenticate.");
		}
	}

}