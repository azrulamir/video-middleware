<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('/entries');
});

Route::post('login', 'Auth\AuthController@login');
Route::get('login',  'Auth\AuthController@showLoginForm');
Route::get('logout', 'Auth\AuthController@logout');

Route::group(['middleware' => ['web']], function () {
    
    Route::post('/entries/upload', 'EntriesController@upload');
    Route::get('/entries', 'EntriesController@index');
    Route::get('/entries/{id}', 'EntriesController@show');
    Route::get('/progress', 'EntriesController@progress');

});

Route::get('/cron', 'CronController@gn4');