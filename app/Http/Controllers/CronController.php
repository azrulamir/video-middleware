<?php

namespace App\Http\Controllers;

use App\Entries;
use App\EntriesGN4;
use App\Jobs\SubmitGN4;
use Illuminate\Http\Request;
use App\Http\Requests;

class CronController extends Controller
{
    /**
     * Check for youtube and ooyala upload and dispatch GN4 job
     *
     * @return 
     */
    public function gn4()
    {
        $entries_gn4 = EntriesGN4::where('status', '=', 'pending')->get();

        foreach($entries_gn4 as $entry) {

            $entries = Entries::with('youtube', 'ooyala')->find($entry->entries_id);

            if (($entries->ooyala->status == 'success') && ($entries->youtube->status == 'success')) {
                
                dispatch(new SubmitGN4($entries));

            }

        }

    }
}
