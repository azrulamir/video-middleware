<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Log;
use Storage;
use App\Entries;
use App\EntriesOoyala;
use App\EntriesYoutube;
use App\EntriesGN4;
use App\Jobs\UploadOoyala;
use App\Jobs\UploadYoutube;
use Illuminate\Http\Request;
use App\Http\Requests;

class EntriesController extends Controller
{

    /**
     * Instantiate controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

	/**
     *  Display list of upload entries
     * 
     * @return void
     */
    public function index()
    {
        $entries = Entries::with('youtube', 'ooyala')->orderBy('created_at', 'DESC')->paginate(10);

    	return view('entries/index')->with([
            'entries' => $entries
        ]);
    }

    /**
     * Show instance detail
     * 
     * @return void
     */
    public function show($id)
    {
        $entry = Entries::with('youtube', 'ooyala', 'gn4')->find($id);

        return view('entries/show')->with([
            'entry' => $entry
        ]);
    }

    /**
     * Receive upload request
     * 
     * @return void
     */
    public function upload(Request $request)
    {

    	$original_name = $request->file('file')->getClientOriginalName();
    	$datetime = new \DateTime();
    	$timestamp = $datetime->getTimestamp();
    	$extension = $request->file('file')->extension();
    	$new_name = $timestamp . "." . $extension;
    	$upload_path = $request->file('file')->move(storage_path('app/uploads'), $new_name);
    	$size = filesize($upload_path);

    	$entry = new Entries;
    	$entry->ori_filename = $original_name;
    	$entry->filename = $new_name;
    	$entry->title = $request->title;
    	$entry->description = $request->description;
    	$entry->size = $size;
    	$entry->save();

        // Create upload job for Ooyala
        $entry_ooyala = new EntriesOoyala;
        $entry_ooyala->entries_id = $entry->id;
        $entry_ooyala->status = 'pending';
        $entry_ooyala->save();

    	dispatch(new UploadOoyala($entry));

        // Create upload job for Youtube
        $entry_youtube = new EntriesYoutube;
        $entry_youtube->entries_id = $entry->id;
        $entry_youtube->status = 'pending';
        $entry_youtube->save();

        dispatch(new UploadYoutube($entry));

        // Create request entry to GN4
        $entry_gn4 = new EntriesGN4;
        $entry_gn4->entries_id = $entry->id;
        $entry_gn4->status = 'pending';
        $entry_gn4->save();

        // Return JSON response
    	$json = array(
			'files' => array(
				"original_name" => $original_name,
				"new_name" => $new_name,
				"extension" => $extension,
				"size" => $size,
				"status" => 'success',
				"time" => $timestamp,
				"title" => $request->title,
				"description" => $request->description
			)
		);

		return response()->json($json);

    }

    /**
     * Supply progress report
     *
     * @param $ids array ID of entries
     */
    public function progress(Request $request)
    {
        $id = $request->id;
        $type = $request->type;

        if ($type == "ooyala") {
            $progress = EntriesOoyala::find($request->id);
        }
        else if ($type == "youtube") {
            $progress = EntriesYoutube::find($request->id);
        }

        return $progress;
    }

}
