<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entries extends Model
{
    
    public function ooyala()
    {
    	return $this->hasOne('App\EntriesOoyala');
    }

    public function youtube()
    {
    	return $this->hasOne('App\EntriesYoutube'); 
    }

    public function gn4()
    {
    	return $this->hasOne('App\EntriesGN4'); 
    }
}
