<?php

namespace App\Jobs;

use Log;
use Storage;
use App\Services\GN4;
use App\Entries;
use App\EntriesYoutube;
use App\EntriesOoyala;
use App\EntriesGN4;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubmitGN4 extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $entries;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Entries $entries)
    {
        $this->entries = $entries;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $upload_entry = $this->entries;
        $entry_ooyala = EntriesOoyala::find($upload_entry->id);
        $entry_youtube = EntriesYoutube::find($upload_entry->id);
        $fileid = pathinfo(storage_path('app/uploads/' . $upload_entry->filename), PATHINFO_FILENAME);

        // Generate thumbnail image and upload to S3
        exec('/usr/bin/ffmpeg -i /var/www/html/middleware/storage/app/uploads/'. $upload_entry->filename .' -ss 00:00:05.00 -vframes 1 -vf scale=320:-1 /var/www/html/middleware/storage/app/thumbnails/'. $fileid .'.png -y 2>&1');
        $thumbnail_file = Storage::get('thumbnails/'. $fileid .'.png');
        $response = Storage::disk('s3')->put('thumbnails/'. $fileid .'.png', $thumbnail_file);

        // Generate preview image and upload to S3
        exec('/usr/bin/ffmpeg -i /var/www/html/middleware/storage/app/uploads/'. $upload_entry->filename .' -ss 00:00:05.00 -vframes 1 -vf scale=640:-1 /var/www/html/middleware/storage/app/previews/'. $fileid .'.png -y 2>&1');
        $preview_file = Storage::get('previews/'. $fileid .'.png');
        $response = Storage::disk('s3')->put('previews/'. $fileid .'.png', $preview_file);

        // Compile data into array
        $data = [
            'id' => $upload_entry->id,
            'filename' => $upload_entry->filename,
            'title' => $upload_entry->title,
            'description' => $upload_entry->description,
            'youtubeid' => $entry_youtube->asset_id,
            'ooyalaid' => $entry_ooyala->asset_id,
            'fileid' => $fileid
        ];

        $entry_gn4 = EntriesGN4::find($upload_entry->id);
        $entry_gn4->status = 'queued';
        $entry_gn4->save();

        $gn4 = new GN4\GN4Api();
        $response = $gn4->send($data);
    }
}
