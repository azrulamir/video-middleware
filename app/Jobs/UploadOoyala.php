<?php

namespace App\Jobs;

use App\Services\Ooyala\OoyalaApi;
use App\Entries;
use App\EntriesYoutube;
use App\EntriesGN4;
use App\Jobs\SubmitGN4;
use Storage;
use App\Jobs\Job;
use Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UploadOoyala extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $entries;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Entries $entries)
    {
        $this->entries = $entries;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $upload_entry = $this->entries;

        $api = new OoyalaApi();

        $path = storage_path() . '/app/uploads/' . $upload_entry->filename;
        $size = Storage::size('uploads/' . $upload_entry->filename);
        
        $params = [
            "file_name" => (string) $upload_entry->filename,
            "name" => (string) $upload_entry->title,
            "description" => (string) $upload_entry->description,
            "asset_type" => "video",
            "file_size" => (string) $size
        ];

        $upload = $api->upload($path, $upload_entry->id, $params);

        if ($upload) {
            dispatch(new SubmitGN4($upload_entry));
        }

    }
}
