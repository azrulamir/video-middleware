<?php

namespace App\Jobs;

use Youtube;
use App\Entries;
use App\EntriesOoyala;
use App\EntriesGN4;
use App\Jobs\SubmitGN4;
use Log;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UploadYoutube extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $entries;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Entries $entries)
    {
        $this->entries = $entries;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $upload_entry = $this->entries;

        $path = storage_path() . '/app/uploads/' . $upload_entry->filename;
        $params = [
            'title' => (string) $upload_entry->title,
            'description' => (string) $upload_entry->description
        ];

        $upload = Youtube::upload($path, $params, $upload_entry->id);

        if ($upload) {
            dispatch(new SubmitGN4($upload_entry));
        }
    }
}
