<?php

namespace App\Events;

use App\Entries;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CheckUpload extends Event
{
    use SerializesModels;

    public $entries;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Entries $entries)
    {
        $this->entries = $entries;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
