<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use App\Entries;
use App\EntriesYoutube;
use App\EntriesOoyala;

class ClearStorage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'storage:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove videos, preview images and XML in storage.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $videos = Storage::files('uploads');
        
        foreach ($videos as $video => $path) {
            
            $fname = basename($path);
            $id = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fname);
            
            $entry = Entries::where('filename', '=', $fname)->first();
            $entry_id = $entry->id;

            $oo = EntriesOoyala::find($entry_id)->status;
            $yt = EntriesYoutube::find($entry_id)->status;

            if ($oo == 'success' && $yt == 'success') {
                Storage::delete('uploads/'.$fname);
                Storage::delete('xml/'.$id.'.xml');
                Storage::delete('previews/'.$id.'.png');
                Storage::delete('thumbnails/'.$id.'.png');
            }

        }
    }
}
