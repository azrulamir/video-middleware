<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntriesGN4 extends Model
{
    protected $table = 'entries_gn4';

    protected $primaryKey = 'entries_id';

    public function entries()
    {
    	return $this->belongsTo('Entries');
    }
}
