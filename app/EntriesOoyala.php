<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntriesOoyala extends Model
{
    protected $table = 'entries_ooyala';

    protected $primaryKey = 'entries_id';

    public function entries()
    {
    	return $this->belongsTo('Entries');
    }
}
