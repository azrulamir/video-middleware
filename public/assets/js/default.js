// Modal settings
$("#uploadModal").modal({
    'backdrop': 'static',
    'show': false
});

$('.add-entry').click(function() {
   $('#uploadModal').modal('show'); 
});

// Progress checker
setInterval(function(){

    $( ".pending, .processing" ).each(function( index ) {

        var progressbar = $(this);
        var id = $(this).attr('data-id');
        var type =  $(this).attr('data-type');
        
        $.ajax({
            url: "/progress",
            type: "GET",
            data: {
                "id": id,
                "type": type
            },
            success: function(result) {

                if (result.status == 'pending') {
                    progressbar
                        .removeClass('active')
                        .addClass('pending')
                        .parent().parent().siblings('td.status').html('<span class="label label-default">Pending</span>');
                }
                else if (result.status == 'processing') {
                    progressbar
                        .addClass('active processing progress-bar-default')
                        .removeClass('pending progress-bar-success')
                        .parent().parent().siblings('td.status').html('<span class="label label-info">Processing</span>');
                }
                else if (result.status == 'success') {
                    progressbar
                        .addClass('progress-bar-success')
                        .removeClass('active pending processing progress-bar-default')
                        .parent().parent().siblings('td.status').html('<span class="label label-success">Success</span>');
                }
                else {
                    progressbar
                        .removeClass('active pending processing')
                        .parent().parent().siblings('td.status').html('<span class="label label-danger">Failed</span>');
                }

                progressbar
                    .css("width", result.progress+'%')
                    .parent().parent().siblings('td.percentage').html(result.progress+'%');


            }
        });

    });

}, 1200);


// fileupload function
$(function () {

    'use strict';

    $('#fileupload').fileupload({
        url: "/entries/upload",
        dataType: 'json',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(mov|avi|mpe?g|mp4|3gpp)$/i,
        maxFileSize: 600000000,
        maxNumberOfFiles: 1,
        submit: function (e, data) {

            var type = data.files[0].type;
            var size = data.files[0].size;
            var title = $('#title').val();

            if (title == '') {
                $('#fileupload').prop('disabled', false);
                $('#title').parent().addClass('has-error');

                return false;
            }

            var k = $('<button/>')
                .text('Cancel upload')
                .addClass('btn btn-danger')
                .click(function () {
                    data.abort();
                    k.remove();
                });
            $('#abort').html(k);

            $('.progress-upload').show();
            $('.progress-upload-bar')
                .removeClass('active')
                .attr('aria-valuenow', 0)
                .text('0%').css('width', '0%');

            $('#upload').hide();

            if (size > 600000000) {
                $('#files')
                    .html($('<span/>')
                    .text(data.files[0].name + ' - File size exceeded 600MB upload limit.')
                    .css({'color' : '#EC7063', 'font-weight' : 'bold'}));

                $('#fileupload').prop('disabled', false);
                $('#abort').html('');

                return false;
            }

            else if (type !== "video/mp4" && type !== "video/quicktime" && type != "video/avi" && type != "video/mpeg" && type != "video/3gpp") {

                $('#files')
                    .html($('<span/>')
                    .text(data.files[0].name + ' - File type not supported.')
                    .css({'color' : '#EC7063', 'font-weight' : 'bold'}));

                $('#fileupload').prop('disabled', false);
                $('#abort').html('');

                return false;
            }

            else {
                $('#files')
                    .html($('<span/>')
                    .text(data.files[0].name)
                    .css({'color' : '#566573', 'font-weight' : 'bold'}));

                $('#fileupload').prop('disabled', false);

            }

        }

    }).on('fileuploadadd', function (e, data) {           

        $("#upload").off('click').on('click', function () {
            data.submit();
        });

        $('#abort').html('');
        $('#files')
            .html($('<span/>')
            .text('You selected : ' + data.files[0].name)
            .css({'color' : '#566573', 'font-weight' : 'bold'}));
        $('.progress-upload').hide();

    }).on('fileuploaddone', function (e, data) {

        $('#files')
            .html($('<span/>')
            .text('Upload complete.')
            .css({'color' : '#93C54B', 'font-weight' : 'bold'}));

    	console.log(data.result.files);

    	$('.progress-upload-bar')
    		.removeClass('active')
    		.attr('aria-valuenow', 100)
    		.text('100%').css('width', '100%');

        $('#fileupload').prop('disabled', false);
        $('#abort').html('');

        $("#upload").off('click');

        window.location = "https://video.nstp.com.my/entries";

    }).on('fileuploadfail', function (e, data) {

        $('#files')
            .html($('<span/>')
            .text('Upload stopped.')
            .css({'color' : '#EC7063', 'font-weight' : 'bold'}));
        
        $('#fileupload').prop('disabled', false);
        $('#abort').html('');
        $('.progress-upload-bar').removeClass('active');

        $('#upload .btn').html('Retry Upload');
        $('#upload').show();

    }).on('fileuploadprogress', function (e, data) {

        var bitrate_kb = Math.round(((data.bitrate / 1024) / 1024) * 100);
        var progress = parseInt(data.loaded / data.total * 100, 10);

        if (progress < 99) {
            $('.progress-upload-bar')
                .addClass('active')
                .attr('aria-valuenow', progress)
                .text(progress + '%')
                .css('width', progress + '%');

            $('#files')
                .html($('<span/>')
                .text('Upload speed : '+bitrate_kb+'KB / sec')
                .css({'color' : '#566573', 'font-weight' : 'bold'}));
        }
        else {
            $('#files')
                .html($('<span/>')
                .text('Your upload is being processed, almost there...')
                .css({'color' : '#566573', 'font-weight' : 'bold'}));
        }

    });

});

// dropzone section
var dropZone = document.getElementById('drop-zone');
dropZone.ondrop = function(e) {
    e.preventDefault();
    this.className = 'upload-drop-zone';
}

dropZone.ondragover = function() {
    this.className = 'upload-drop-zone drop';
    return false;
}

dropZone.ondragleave = function() {
    this.className = 'upload-drop-zone';
    return false;
}

// disable form submit on press enter
$('#uploadform').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});